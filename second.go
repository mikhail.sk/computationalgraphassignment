package main

import (
	"strings"
)

type node struct {
	name        string
	nodeType    string
	value       float32
	expression  string
	derivative  []float32
	connections []*node
}

// func mul(a, b node) node {
// 	// fmt.Printf("%.2f*%.2f=%.2f ---> ", a, b, a*b)
// 	var n node
// 	n.name = a.name+"*"+b.name
// 	n.value = a.value * b.value
// 	n.derivative = []float32{b.value, a.value}
// 	return n
// }

// func add(a, b node) node {
// 	fmt.Printf("%.2f+%.2f=%.2f ---> ", a, b, a+b)
// 	return a + b
// }

// func sqr(a node) node {
// 	fmt.Printf("%.2f^2=%.2f ---> ", a, a*a)
// 	return a * a
// }

func processString(s string) string {
	s = strings.ReplaceAll(s, "(", " ")
	s = strings.ReplaceAll(s, ",", " ")
	s = strings.ReplaceAll(s, ")", " ")
	s = strings.ReplaceAll(s, "  ", " ")
	s = strings.ReplaceAll(s, "   ", " ")
	s = strings.ReplaceAll(s, "    ", " ")
	s = strings.TrimSpace(s)

	return s
}

func main() {

	yHat := "add(mul(sqr(add(x,b)),a),c)"
	// yHat := "add(add(mul(a,sqr(x)),mul(b,x)),c)"
	// yHat := "add(mul(c,sqr(x)),sqr(add(a,mul(b,x))))"

	yHat_ := processString(yHat)
	z := strings.Split(yHat_, " ")

	paramNames := []string{"a", "b", "c"}
	paramValues := []float32{0.2, 0.5, 1}

	x=2
	y=5

	xCount := strings.Count(yHat, "x")
	xIndices := make([]int, xCount)
	xIndices[0] = strings.Index(z, "x")
	if xCount > 1 {
		xIndices[1] = strings.Index(z[xIndices[0]:])
	}

	values = make(map[string]float32{"x":x,"y":y})
	for j:=0;j<len(paramValues);j++{
		values[paramNames[j]] = paramValues[j]
	}

	nodes := make([]node, len(z))

	for i:=0;i<len(nodes);i++{
		nodes[i].name = z[i] //String representation of item

		if len(z[i]) > 1{	//Classify type of node, x, op, or var
			nodes[i].nodeType = "op"
		} else if z[i] == "x"{
			nodes[i].nodeType = "x"
		} else {
			nodes[i].nodeType = "var"
		}

		if nodes[i].nodeType == "op"{
			nodes[i].value = 0.0
		} else {
			nodes[i].value = values[z[i]]
		}

		nodes[i].expression = 

		nodes[i].derivative = 
		
		nodes[i].connections = 
	}

}
