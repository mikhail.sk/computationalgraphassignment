// package main

// import (
// 	"fmt"
// 	"strings"
// )

// func mul(a, b float64) float64 {
// 	fmt.Printf("%.2f*%.2f=%.2f ---> ", a, b, a*b)
// 	return a * b
// }

// func add(a, b float64) float64 {
// 	fmt.Printf("%.2f+%.2f=%.2f ---> ", a, b, a+b)
// 	return a + b
// }

// func sqr(a float64) float64 {
// 	fmt.Printf("%.2f^2=%.2f ---> ", a, a*a)
// 	return a * a
// }

// func processString(s string) string {
// 	s = strings.ReplaceAll(s, "(", " ")
// 	s = strings.ReplaceAll(s, ",", " ")
// 	s = strings.ReplaceAll(s, ")", " ")
// 	s = strings.ReplaceAll(s, "  ", " ")
// 	s = strings.ReplaceAll(s, "   ", " ")
// 	s = strings.ReplaceAll(s, "    ", " ")
// 	s = strings.TrimSpace(s)

// 	return s
// }

// func main() {

// 	var x float64
// 	var y float64

// 	x = 1
// 	y = 5

// 	yHat := "add(mul(sqr(add(x,b)),a),c)"
// 	// yHat := "add(add(mul(a,sqr(x)),mul(b,x)),c)"
// 	// yHat := "add(mul(c,sqr(x)),sqr(add(a,mul(b,x))))"

// 	paramNames := []string{"a", "b", "c"}
// 	paramValues := []float64{0.2, 0.5, 1}

// 	tP := map[string]float64{"x": x, "y": y}

// 	for q := 0; q < len(paramNames); q++ {
// 		tP[paramNames[q]] = paramValues[q]
// 	}

// 	noOfOps := strings.Count(yHat, ")")

// 	yHat1 := processString(yHat)

// 	z := strings.Split(yHat1, " ")

// 	first := noOfOps //first trainable parameter or x index

// 	operations := make([]float64, noOfOps+2)
// 	differentiations := make([][]float64, len(operations))
// 	for i := range differentiations {
// 		differentiations[i] = make([]float64, 2)
// 	}

// 	fmt.Println(z)

// 	paramPos := make([]int, len(paramNames))

// 	for i := range paramPos {
// 		param := paramNames[i]
// 		substringSlice := strings.SplitAfterN(yHat, param, -1)
// 		noOfParentheses := strings.Count(substringSlice[len(substringSlice)-1], ")")

// 		paramPos[i] = noOfOps + 2. - noOfParentheses - 1.
// 	}
// 	for i := 0; i < noOfOps; i++ {
// 		op := z[first-1-i]
// 		if i == 0 {
// 			switch op {
// 			case "mul":
// 				operations[i] = mul(tP[z[first+i]], tP[z[first+i+1]])
// 				differentiations[i][0] = tP[z[first+i]]
// 				differentiations[i][1] = tP[z[first+i+1]]
// 			case "add":
// 				operations[i] = add(tP[z[first+i]], tP[z[first+1+i]])
// 				differentiations[i][0] = 1
// 				differentiations[i][1] = 1
// 			case "sqr":
// 				operations[i] = sqr(tP[z[first+i]])
// 				differentiations[i][0] = 2 * tP[z[first+i]]
// 				differentiations[i][1] = 1000
// 			}
// 		} else {
// 			switch op {
// 			case "mul":
// 				operations[i] = mul(operations[i-1], tP[z[first+i+1]])
// 				differentiations[i][0] = tP[z[first+1+i]]
// 				differentiations[i][1] = operations[i-1]
// 			case "add":
// 				operations[i] = add(operations[i-1], tP[z[first+1+i]])
// 				differentiations[i][0] = 1
// 				differentiations[i][1] = 1
// 			case "sqr":
// 				operations[i] = sqr(operations[i-1])
// 				differentiations[i][0] = 2 * operations[i-1]
// 				differentiations[i][1] = 1000
// 			}
// 		}
// 		if i == noOfOps-1 {
// 			operations[i+1] = y - operations[i]
// 			fmt.Printf("%.2f-%.2f=%.2f ---> ", y, operations[i], y-operations[i])
// 			operations[i+2] = sqr(operations[i+1])
// 			fmt.Printf("end")
// 			differentiations[i+1][0] = -1
// 			differentiations[i+1][1] = 1
// 			differentiations[i+2][0] = 2 * operations[i+1]
// 			differentiations[i+2][1] = 1000

// 		}
// 	}

// 	// for i := range paramPos {
// 	// 	fmt.Println(paramNames[i], ": pos", paramPos[i], "diff:", differentiations[paramPos[i]-1][1])
// 	// }

// 	paramGradients := make([]float64, len(paramNames))
// 	genGradients := make([]float64, len(operations))

// 	genGradients[len(operations)-1] = differentiations[len(operations)-1][0]

// 	for i := len(paramNames) - 1; i >= 0; i-- {

// 		paramGradients[i] = differentiations[len(operations)-1][0]

// 		for j := len(operations) - 2; j >= 0; j-- {
// 			if paramPos[i]-1 == j {
// 				paramGradients[i] *= differentiations[j][1]
// 				break
// 			} else {
// 				if genGradients[j] != 0 {
// 					paramGradients[i] = genGradients[j]
// 				} else {
// 					genGradients[j] = differentiations[j][0] * genGradients[j+1]
// 					paramGradients[i] *= differentiations[j][0]
// 				}

// 			}

// 			// fmt.Println(paramGradients[i], differentiations[j])
// 		}
// 		genGradients[0] = differentiations[0][0] * genGradients[1]
// 	}
// 	// fmt.Println(paramGradients)
// 	for i := range paramGradients {
// 		fmt.Printf("d/d%s = %.2f\n", paramNames[i], paramGradients[i])
// 	}
// 	fmt.Println("genGradients: ", genGradients)
// 	fmt.Println("Differentiations of each operation:", differentiations)
// }
