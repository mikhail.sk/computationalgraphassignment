// package main;

// import (
// 	"fmt"

// );

// type Node struct {
//     prev *Node
//     next *Node
//     key  interface{}
// }

// type List struct {
//     head *Node
//     tail *Node
// }

// func (L *List) Insert(key interface{}) {
//     list := &Node{
//         next: L.head,
//         key:  key,
//     }
//     if L.head != nil {
//         L.head.prev = list
//     }
//     L.head = list

//     l := L.head
//     for l.next != nil {
//         l = l.next
//     }
//     L.tail = l
// }

// func (l *List) DisplayCGraph() {
//     list := l.head
//     for list != nil {
//         fmt.Printf("%s ->", list.key)
//         list = list.next
//     }
//     fmt.Println()
// }

// func (l *List) DisplayCGraph2() {
//     list := l.tail
//     for list != nil {
//         fmt.Printf("%s", list.key)
//         list = list.prev

//         if list != nil {
//         	fmt.Printf(" -> ");
//         }
//     }
//     fmt.Println()
// }
// // func Display(list *Node) {
// //     for list != nil {
// //         fmt.Printf("%c ->", list.key)
// //         list = list.next
// //     }
// //     fmt.Println()
// // }

// func main() {
//     //var ss1 string   =  "ax^2+bx+c";
//     var ss1  string  =  "(a*(x+b)^2+c)^2";
//     var expr1 string =  "add(add(mul(sqr(x),a),mul(x,b)),c)";
//     //var expr2 string =  "sqr(add(mul(sqr(add(x,b)),a),c))";

//     fmt.Println("Given Expression: ",ss1," => ",expr1);
//     fmt.Println("format convention: variable variable operand/variable operand");
//     fmt.Println("Computational Graph");
//     fmt.Printf("\n\n");

//     CGraph  :=  [1000] List{}

//     indexOfCGraph := 0;

//     mymap := make(map[int] string)

//     mymap[40] = "(";
//     mymap[41] = ")";
//     mymap[42] = "*";
//     mymap[43] = "+";
//     mymap[50] = "2";
//     mymap[94] = "^";
//     mymap[97] = "a";
//     mymap[98] = "b";
//     mymap[99] = "c";
//     mymap[100] = "d";
//     mymap[101] = "e";
//     mymap[120] = "x";
//     mymap[121] = "y";

//     var stk []string // the most naive version of stack implemeantation I could find is this one

//    // keeping everything inside stk (stack)
//     for i := 0; i<len(ss1); i++ {

//     	switch ss1[i] {
//     		case 40: { // (

//     			stk = append(stk, "(");
//     		}
//     		case 41: { // )

//     			stk = append(stk, ")");
//     		}
//     		case 42: { // *

//     			stk = append(stk, "*");
//     		}
//     		case 43: { // +

//     			stk = append(stk, "+");
//     		}
//     		case 50: { // 2

//     			stk = append(stk, "2");
//     		}
// 	    	case 94: {
//     			stk = append(stk, "^")
// 	    	}
//     		case 97: { // a

//     			stk = append(stk, "a");
//     		}

//     		case 98: {// b

//     			stk = append(stk, "b");
//     		}
//     		case 99: {// c

//     			stk = append(stk, "c");
//     		}
// 	   		case 100: {// d

//     			stk = append(stk, "d");
//     		}

// 	   		case 101: {// e

//     			stk = append(stk, "e");
//     		}

//        		case 120: {// x

//     			stk = append(stk, "x");
//     		}
//     		case 121: {// y

//     			stk = append(stk, "y");
//     		}
//    		}
// 	}

// /* will use 2 stack technique to parse brackets and separate x, a, b ,c,d,*,+,^,2*/

// 	var stk2 []string;

// 	for  len(stk)> 0 {
//     	n := len(stk) - 1 // index of Top element

//    		str := stk[n]; // string at the top

//    		stk = stk[:n] // Pop

//    		if str == "(" {

//    			if len(stk2) >= 1 {
//    				stk2Len := len(stk2) - 1;
//    				for stk2[stk2Len] != ")" {
// 	   				sss := stk2[stk2Len];
//    					if len(sss) == 1 {
//    						CGraph[indexOfCGraph].Insert(sss);
//    						stk2 = stk2[:stk2Len]; // popoing top element of stk2
//    						stk2Len--;
//    					}

//    				}
//    				indexOfCGraph++;
//    				stk2 = stk2[:stk2Len];
//    			}
//    		} else {//
//    			stk2 = append(stk2, str);
//    		}

// 	}

// 	   		//now to desplay the computational graph in
// 	   		//variable variable operand (style)

//    		for itr := 0; itr < indexOfCGraph; itr++ {
//    			CGraph[itr].DisplayCGraph2();
//    		}

// }
